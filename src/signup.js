import React, { Component } from "react";
import "./App.scss";
import userImage from "./user.png";
import emailImage from "./email.png";
import passwordImage from "./password.png";
import confirmImage from "./confirm.png";
import wrongImage from "./wrong.png";
import { Link } from "react-router-dom";

class Signup extends Component {
  state = {
    email: "",
    userName: "",
    password: "",
    nameCorrect: false,
    emailCorrect: false,
    passwordCorrect: false,
    userVerImg: "",
    passVerImg: "",
    emailVerImg: "",
    emptyPlaceHolder: "ㅤ",
    userError: "ㅤ",
    emailError: "ㅤ",
    passwordError: "ㅤ",
    button: "Sign Up",
  };

  validateUserName(eve) {
    this.setState({ userError: this.state.emptyPlaceHolder });
    let val = eve.target.value;
    const last = val[val.length - 1];
    const regex = /^[a-zA-Z ]*$/;
    val = val.trim();
    if (last == " " && val.length > 0) {
      val += " ";
    }
    if (!val.match(regex)) {
      this.setState({ userError: "Name can only contain alphabets or space" });
    } else if (val.length > 25) {
      this.setState({ userError: "Name limit is 25 characters" });
    } else {
      this.setState({ userName: val });
    }
    if (this.userError == this.emptyPlaceHolder && this.userName != "") {
      this.setState({ nameCorrect: true });
    }
  }

  validateEmail(eve) {
    this.setState({ emailError: this.state.emptyPlaceHolder });
    let val = eve.target.value;
    const last = val[val.length - 1];
    const regex =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    val = val.trim().toLowerCase();
    if (last == " " && val.length > 0) {
      this.setState({ emailError: "Spaces are not allowed in email" });
      return;
    }
    if (!val.match(regex)) {
      this.setState({ emailError: "Please enter a valid email" });
    }
    if (val.length > 100) {
      this.setState({ emailError: "Email limit is 100 characters" });
      return;
    }
    this.setState({ email: val });
    if (
      this.state.emailError === this.state.emptyPlaceHolder &&
      this.state.email != ""
    ) {
      fetch(`http://localhost:3000/users/${val}`)
        .then(res => res.json())
        .then(res => {
          if (!res.found) {
            this.setState({ emailCorrect: true });
          } else {
            this.setState({ emailError: "Email already exists" });
          }
        });
    }
  }

  validatePassword(eve) {
    this.setState({ passwordError: this.state.emptyPlaceHolder });
    const val = eve.target.value;
    const regexNum = /\d/;
    const regexLetter = /^.*[a-zA-Z]+.*$/;
    if (val.length < 8) {
      this.setState({
        passwordError: "Should atleast be 8 characters",
      });
    } else if (!val.match(regexNum)) {
      this.setState({
        passwordError: "Please include at least one number",
      });
    } else if (!val.match(regexLetter)) {
      this.setState({
        passwordError: "Please include at least one letter",
      });
    }
    this.setState({ password: val });
    if (this.passwordError == this.emptyPlaceHolder && this.password != "") {
      this.setState({ passwordCorrect: true });
    }
  }

  signup() {
    if (
      this.state.nameCorrect &&
      this.state.emailCorrect &&
      this.state.passwordCorrect
    ) {
      this.setState({ button: "Signing up..." });
      const properties = {
        method: "post",
        headers: {
          mode: "no-cors",
          "Access-Control-Request-Method": "POST",
          "Access-Control-Allow-Origin": "http://localhost:3000",
          "Access-Control-Request-Headers": "Content-Type, Authorization",
          "Access-Control-Allow-Credentials": "true",
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          name: this.state.userName,
          email: this.state.email,
          password: this.state.password,
        }),
      };
      fetch("http://localhost:3000/users", properties)
        .then(res => {
          return res.json();
        })
        .then(res => {
          if (res.result) {
            this.setState({ button: "Sign up successful" });
          }
        });
    } else {
      if (
        !this.state.nameCorrect &&
        this.state.userError === this.state.emptyPlaceHolder
      ) {
        this.setState({ userError: "Please enter the name" });
      }
      if (
        !this.state.emailCorrect &&
        this.state.emailError === this.state.emptyPlaceHolder
      ) {
        this.setState({ emailError: "Please enter the email" });
      }
      if (
        !this.state.passwordCorrect &&
        this.state.passwordError === this.state.emptyPlaceHolder
      ) {
        this.setState({ passwordError: "Please enter a password" });
      }
    }
  }

  render() {
    return (
      <div className="App">
        <div className="form-container">
          <div className="box">
            <img src={userImage} className="icon" alt="userName icon" />
            <input
              value={this.state.userName}
              onChange={this.validateUserName.bind(this)}
              className="input"
              type="text"
              placeholder="Name"
            ></input>
            <img
              src={this.state.userVerImg}
              className="icon verIcon"
              alt="verification icon"
            />
          </div>
          <p className="error-text">{this.state.userError}</p>
          <div className="box">
            <img src={emailImage} className="icon" alt="email icon" style={{"maxWidth": "40px"}}/>
            <input
              value={this.state.email}
              onChange={this.validateEmail.bind(this)}
              className="input"
              type="text"
              placeholder="Email"
            ></input>
            <img
              src={this.state.emailVerImg}
              className="icon verIcon"
              alt="verification icon"
            />
          </div>
          <p className="error-text">{this.state.emailError}</p>
          <div className="box">
            <img src={passwordImage} className="icon" alt="password icon" />
            <input
              value={this.state.password}
              onChange={this.validatePassword.bind(this)}
              className="input"
              type="password"
              placeholder="Password"
            ></input>
            <img
              src={this.state.passVerImg}
              className="icon verIcon"
              alt="verification icon"
            />
          </div>
          <p className="error-text">{this.state.passwordError}</p>
          <button className="button" onClick={this.signup.bind(this)}>
            {this.state.button}
          </button>
          <div className="divider">
            <div className="line"></div>
            <p>or</p>
            <div className="line"></div>
          </div>
          <Link style={{ minWidth: "100%" }} to="/login">
            <button className="button">Login</button>
          </Link>
        </div>
      </div>
    );
  }
}

export default Signup;
