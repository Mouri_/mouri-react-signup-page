import React, { Component } from "react";
import "./App.scss";
import Login from "./login"
import Signup from "./signup";
import {Route, BrowserRouter as Router} from "react-router-dom"

class App extends Component {

  render() {
    return (
      <Router>
        <Route path="/login" component={Login}/>
        <Route path="/signup" component={Signup}/>
        <Route path="/" exact component={Login}/>
      </Router>
      
    );
  }
}

export default App;
