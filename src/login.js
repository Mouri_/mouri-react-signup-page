import React, { Component } from "react";
import "./App.scss";
import emailImage from "./email.png";
import passwordImage from "./password.png";
import confirmImage from "./confirm.png";
import wrongImage from "./wrong.png";
import { Link } from "react-router-dom";

class Signup extends Component {
  state = {
    email: "",
    userName: "",
    password: "",
    nameCorrect: false,
    emailCorrect: false,
    passwordCorrect: false,
    userVerImg: "",
    passVerImg: "",
    emailVerImg: "",
    emptyPlaceHolder: "ㅤ",
    userError: "ㅤ",
    emailError: "ㅤ",
    passwordError: "ㅤ",
    button: "Login",
  };

  validateEmail(eve) {
    this.setState({ emailError: this.state.emptyPlaceHolder });
    let val = eve.target.value;
    const last = val[val.length - 1];
    const regex =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    val = val.trim().toLowerCase();
    if (last == " " && val.length > 0) {
      this.setState({ emailError: "Spaces are not allowed in email" });
      return;
    }
    if (!val.match(regex)) {
      this.setState({ emailError: "Please enter a valid email" });
    }
    if (val.length > 100) {
      this.setState({ emailError: "Email limit is 100 characters" });
      return;
    }
    this.setState({ email: val });
    if (
      this.state.emailError == this.state.emptyPlaceHolder &&
      this.state.email != ""
    ) {
      this.setState({ emailCorrect: true });
    }
  }

  validatePassword(eve) {
    this.setState({ passwordError: this.state.emptyPlaceHolder });
    const val = eve.target.value;
    this.setState({ password: val });
    if (this.state.password != "") {
      this.setState({ passwordCorrect: true });
    }
  }

  login() {
    if (this.state.emailCorrect && this.state.passwordCorrect) {
      const properties = {
        method: "post",
        headers: {
          // mode: "no-cors",
          "Access-Control-Request-Method": "POST",
          "Access-Control-Allow-Origin": "http://localhost:3000",
          "Access-Control-Request-Headers": "Content-Type, Authorization",
          "Access-Control-Allow-Credentials": "true",
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: this.state.email,
          password: this.state.password,
        }),
      };
      fetch("http://localhost:3000/login", properties)
        .then(res => {
          return res.json();
        })
        .then(res => {
          if (res.error1) {
            this.setState({ emailCorrect: false });
            this.setState({ emailError: res.error1 });
          } else if (res.error2) {
            this.setState({ passwordError: res.error2 });
          } else if (res.status) {
            this.setState({ button: "Login Successful" });
          }
        });
    } else {
      if (
        !this.state.emailCorrect &&
        this.state.emailError === this.state.emptyPlaceHolder
      ) {
        this.setState({ emailError: "Please enter the email" });
      }
      if (
        !this.state.passwordCorrect &&
        this.state.passwordError === this.state.emptyPlaceHolder
      ) {
        this.setState({ passwordError: "Please enter the password" });
      }
    }
  }

  render() {
    return (
      <div className="App">
        <div className="form-container">
          <p className="error-text">{this.state.userError}</p>
          <div className="box">
            <img src={emailImage} className="icon" alt="email icon" style={{"maxWidth": "40px"}}/>
            <input
              value={this.state.email}
              onChange={this.validateEmail.bind(this)}
              className="input"
              type="text"
              placeholder="Email"
            ></input>
            <img
              src={this.state.emailVerImg}
              className="icon verIcon"
              alt="verification icon"
            />
          </div>
          <p className="error-text">{this.state.emailError}</p>
          <div className="box">
            <img src={passwordImage} className="icon" alt="password icon" />
            <input
              value={this.state.password}
              onChange={this.validatePassword.bind(this)}
              className="input"
              type="password"
              placeholder="Password"
            ></input>
            <img
              src={this.state.passVerImg}
              className="icon verIcon"
              alt="verification icon"
            />
          </div>
          <p className="error-text">{this.state.passwordError}</p>
          <button className="button" onClick={this.login.bind(this)}>
            {this.state.button}
          </button>
          <div className="divider">
            <div className="line"></div>
            <p>or</p>
            <div className="line"></div>
          </div>
          <Link style={{ minWidth: "100%" }} to="/signup">
            <button className="button">Signup</button>
          </Link>
        </div>
      </div>
    );
  }
}

export default Signup;
