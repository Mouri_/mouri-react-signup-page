const sequelize = require("./database");
require("./models/users");

sequelize
  .sync({ force: true })
  .then(res => console.log("success"))
  .catch(console.log);