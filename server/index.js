require("dotenv").config();

require("./database");

const express = require("express");

const path = require("path");

const cors = require("cors");

const config = require("./config.json");

const userRoute = require("./routers/users");

const authRoute = require("./routers/auth");

const PORT = process.env.PORT || config.PORT;

const app = express();

app.use(cors());

app.use(express.json());

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "GET,POST,OPTIONS,DELETE,PUT");
  next();
});

app.use("/", express.static(path.join(__dirname, "../build")));

app.use("/login", authRoute);

app.use("/users", userRoute);

app.use("/", (req, res) => {
  res.status(200).json({ error: "Bad request" });
});

app.listen(PORT, () => console.log("Express server running on port", PORT));
