const usersModel = require("../models/users");
const router = require("express").Router();

router.post("/", (req, res) => {
  const bcrypt = require("bcrypt");
  const email = req.body.email;
  const password = req.body.password;

  usersModel
    .findAll({
      attributes: ["id", "password"],
      where: {
        email: email,
      },
    })
    .then(result => {
      if (result.length <= 0)
        return res.status(200).json({ error1: "Wrong user name" });
      const user = result[0];
      if (bcrypt.compareSync(password, user.password)) {
        return res.status(200).json({ status: "loggedin" });
      } else {
        return res.status(200).json({ error2: "Wrong password" });
      }
    })
    .catch(err => {
      res.status(500).json({ error: "Something went wrong" });
    });
});

module.exports = router;
