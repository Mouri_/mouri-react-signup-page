const router = require("express").Router();
const bcrypt = require("bcrypt");
const usersModel = require("../models/users");

router.get("/:id", (req, res) => {
  usersModel
  .findAll({
    attributes: ["id"],
    where: {
      email: req.params.id,
    },
  })
    .then(result => {
      if (result.length != 0) return res.status(200).json({found: true});
      res.status(404).json({found: false});
    })
    .catch(err => res.status(500).json({ error: "Internal error" }));
});

router.post("/", (req, res) => {
  const body = req.body;
  const salt = bcrypt.genSaltSync();
  req.body.password = bcrypt.hashSync(body.password, salt);

  usersModel
    .create(req.body)
    .then(result => res.status(201).json({ result: "Success" }))
    .catch(err =>
      res
        .status(409)
        .json({ result: "Failed" })
    );
});

module.exports = router;
